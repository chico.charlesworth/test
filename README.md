# Empty repo to check you can clone Gitlab repositories

### 1. Add SSH Keys on Gitlab

:warning: Skip if you've already setup SSH on Gitlab :sweat_smile:

1. Copy your local SSH key
```
cat ~/.ssh/id_rsa.pub
```
2. Open https://gitlab.com/-/profile/keys, paste the SSH key contents into `Key` & click `Add key`

Any problems with the above steps see https://docs.gitlab.com/ee/user/ssh.html.


### 2. Clone repository

```
git clone git@gitlab.com:chico.charlesworth/test.git
```

**If the above works then it's all good :tada:, if not then get in touch with Chico.**
